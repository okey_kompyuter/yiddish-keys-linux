yiddish-keys-linux
==================

Yiddish keyboard layout for GNU/Linux

Windows version: https://github.com/heyheydanhey/yiddish-keys-windows

OSX version: https://github.com/heyheydanhey/yiddish-keys-osx

More info: http://www.shretl.org


HEY DUMDUM, HEY SCHMO 
---------------------
**Didn't you know you can write Yiddish with a Hebrew layout?**


Like a frozen forest bare of birds and leaves, Yiddish orthography on the internet 
has shed its defining hats, dots and strikes because of the lack of alternatives 
to Modern Hebrew layouts, for which all such things are superfluous. Not only that, 
but Hebrew's prioritization of different letters to Yiddish means that typing is so
frustrating for many that they prefer typing in Latin characters than Hebrew. 
Transliteration hurts the language - it prevents the development of litteracy, 
especially for those who come to the language as students rather than through birth.

This Yiddish keyboard hopes to address this by providing an intuitive and eventually
standard Yiddish keyboard for all major OSs to ultimately, it is hoped, include 
as vanilla feature.

This Linux version so far includes three layouts:

  * a qwerty based layout
  * an Israeli-hebrew based layout
  * an Israeli phonetic based layout

**Installation**

On the *buntus, at least:

 1) RECOMENDED: Back up your /usr/share/X11/xkb directory, just in case you experience any problems after installation.
   
 2) Run ./install.sh
  
 3) Open your keyboard layout dialogue. It might be in Preferences > Keyboard > Keyboard layouts.
 
 4) Add a new layout (+) > search for Yiddish > choose from either a qwerty-based layout or an Israeli-Hebrew-based layout (or go for both, if you're feeling adventurous).
 
 Toggle between your keyboard layouts in the taskbar.
